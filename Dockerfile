FROM golang:1.13.4-alpine3.10 as base
RUN apk add --update --no-cache git
RUN mkdir /library-backend
WORKDIR /library-backend
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 go build -a -ldflags '-d -s -w' -o ./bin/svc ./src/main.go

FROM alpine:3.10
RUN apk add --no-cache ca-certificates
COPY --from=base /library-backend/bin/svc /svc
EXPOSE 8000
ENTRYPOINT ["/svc", "server"]
