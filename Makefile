GO_ENV ?= test

include env/${GO_ENV}.env

PROJECT_NAME = library
PROJECT_VERSION = 0.0.1
GIT_FOLDER = $(shell git rev-parse --show-toplevel)
MAKE_PARAMS = $(filter-out $@,$(MAKECMDGOALS))
DB_URL = mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@tcp(localhost:${MYSQL_PORT})/${MYSQL_DATABASE}?multiStatements=true

# Ignore `No rule to make target` errors
%:
	@echo ""

# Install vendor dependencies
deps:
	@go mod tidy
	@go mod download
	@go mod vendor
.PHONY: deps

# Build service
build:
	@docker build -t ${PROJECT_NAME}:${PROJECT_VERSION} .
.PHONY: build

# Start docker environment
start:
	@docker-compose -f docker-compose/${GO_ENV}.yaml up --detach
.PHONY: start

# Reload docker environment
reload: build
	@docker-compose -f docker-compose/${GO_ENV}.yaml up --detach --force-recreate --no-deps library-svc-${GO_ENV}
.PHONY: reload

# Stop docker environment
stop:
	@docker-compose -f docker-compose/${GO_ENV}.yaml down
.PHONY: stop

# Tail logs
logs:
	@docker-compose -f docker-compose/${GO_ENV}.yaml logs --follow --tail=200
.PHONY: logs

# Run db migrations
migrate:
	@docker run -v ${GIT_FOLDER}/migrations:/migrations --network host migrate/migrate:v4.7.0 -path=/migrations/ -database "${DB_URL}" ${MAKE_PARAMS}
.PHONY: migrate

# Create new db migration file
migration:
	@docker run -v ${GIT_FOLDER}/migrations:/migrations --network host -u `id -u`:`id -g` migrate/migrate:v4.7.0 create -ext sql -dir migrations ${MAKE_PARAMS}
.PHONY: migration

# Run tests
test:
	@export $$(cat env/test.env) && go test -p 1 `go list ./... | grep -v /vendor/`
.PHONY: test
