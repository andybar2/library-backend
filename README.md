# Library Backend

## Install `go`:

Follow the official installation instructions:
- [How to install go compiler](https://golang.org/doc/install)

## Install `docker` and `docker-compose`:

Follow the official installation instructions:
- [How to install docker](https://docs.docker.com/install)
- [How to install docker compose](https://docs.docker.com/compose/install)

## Clone repository:

```bash
git clone git@gitlab.com:andybar2/library-backend.git
cd library-backend
```

## Install project dependencies:

```bash
make deps
```

## Build service:

```bash
make build
```

## Development environment:

```bash
# Run development environment:
GO_ENV=dev make start

# Run db migrations:
GO_ENV=dev make migrate up

# Show logs
GO_ENV=dev make logs

# Reload development environment:
GO_ENV=dev make reload

# Stop development environment:
GO_ENV=dev make stop
```

## Tests:

```bash
# Run test environment:
GO_ENV=test make start

# Run db migrations:
GO_ENV=test make migrate up

# Run tests:
make test

# Stop test environment:
GO_ENV=test make stop
```
