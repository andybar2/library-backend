CREATE TABLE books (
  id            VARCHAR(36) NOT NULL,
  title         VARCHAR(100) NOT NULL,
  author        VARCHAR(100) NOT NULL,
  isbn          VARCHAR(50) NOT NULL,
  description   VARCHAR(200) NOT NULL,
  current_state TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE UNIQUE INDEX books_isbn_udx ON books (isbn);

CREATE INDEX books_title_id_idx ON books (title, id);