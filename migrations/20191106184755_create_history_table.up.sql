CREATE TABLE history (
  id      VARCHAR(36) NOT NULL,
  book_id VARCHAR(36) NOT NULL,
  person  VARCHAR(100) NOT NULL,
  action  TINYINT NOT NULL DEFAULT 0,
  date    DATETIME(3) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT history_book_id_fk FOREIGN KEY (book_id) REFERENCES books (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE INDEX history_book_id_date_idx ON history (book_id, date);