package cmd

import (
	"log"

	"github.com/spf13/cobra"

	"andybar2/library-backend/src/config"
	"andybar2/library-backend/src/service"
	"andybar2/library-backend/src/store"
)

// serverCmd is the command to run the server
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "runs the server process for this service",
	Long:  "will expose an HTTP API for the service",
	RunE:  server,
}

func init() {
	RootCmd.AddCommand(serverCmd)
}

func server(cmd *cobra.Command, args []string) error {
	// Config
	conf := config.New()

	// Store
	st, err := store.New(conf)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer st.Close()

	// Service
	svc := service.New(conf, st)
	return svc.Run()
}
