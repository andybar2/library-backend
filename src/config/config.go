package config

import (
	"os"
)

// Config contains configuration parameters
type Config struct {
	LogLevel      string
	HTTPPort      string
	MySQLHost     string
	MySQLPort     string
	MySQLDatabase string
	MySQLUser     string
	MySQLPassword string
}

// New reads the configuration parameters from the OS' environment variables
func New() *Config {
	return &Config{
		LogLevel:      os.Getenv("LOG_LEVEL"),
		HTTPPort:      os.Getenv("HTTP_PORT"),
		MySQLHost:     os.Getenv("MYSQL_HOST"),
		MySQLPort:     os.Getenv("MYSQL_PORT"),
		MySQLDatabase: os.Getenv("MYSQL_DATABASE"),
		MySQLUser:     os.Getenv("MYSQL_USER"),
		MySQLPassword: os.Getenv("MYSQL_PASSWORD"),
	}
}
