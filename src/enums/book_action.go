package enums

const (
	// BookActionCheckOut is the action used for checking out books from the library
	BookActionCheckOut = iota

	// BookActionCheckIn is the action used for checking in books to the library
	BookActionCheckIn
)
