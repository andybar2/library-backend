package enums

const (
	// BookStateAvailable is the state used for books that are available for check out
	BookStateAvailable = iota

	// BookStateTaken is the state used for books that are not available for check out
	BookStateTaken
)
