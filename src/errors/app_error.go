package errors

import (
	"database/sql"
	"fmt"
	"net/http"
	"strings"

	"github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
)

// AppError is an application custom error
type AppError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Status  int    `json:"-"`
}

// Error returns the string representation of an AppError
func (e *AppError) Error() string {
	return e.Message
}

// Handler returns an echo handler for application errors
func Handler() echo.HTTPErrorHandler {
	return func(err error, ctx echo.Context) {
		appErr := fromError(err)
		ctx.Logger().Error(appErr.Error())

		if appErr.Status == http.StatusInternalServerError {
			// Override message to avoid exposing details about the internal errors
			appErr.Message = "internal server error"
		}

		if err := ctx.JSON(appErr.Status, appErr); err != nil {
			ctx.Logger().Error(err.Error())
		}
	}
}

// fromError returns an AppError from a given generic error
func fromError(err error) *AppError {
	if err == sql.ErrNoRows {
		return &AppError{codeNotFound, "resource not found", http.StatusNotFound}
	}

	switch tErr := err.(type) {
	case *AppError:
		return tErr
	case *echo.HTTPError:
		return &AppError{codeGeneral, strings.ToLower(fmt.Sprintf("%v", tErr.Message)), tErr.Code}
	case *mysql.MySQLError:
		if tErr.Number == 1062 {
			return &AppError{codeAlreadyExists, "resource already exists", http.StatusConflict}
		}
	}

	return &AppError{codeInternal, err.Error(), http.StatusInternalServerError}
}
