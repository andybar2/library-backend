package errors

import (
	"fmt"
	"net/http"
)

const (
	codeGeneral            = 1
	codeInternal           = 2
	codeInputValidation    = 3
	codeNotFound           = 4
	codeAlreadyExists      = 5
	codePreconditionFailed = 6
)

// Internal returns a new AppError as an error
func Internal(message string) error {
	return &AppError{codeInternal, message, http.StatusInternalServerError}
}

// InputValidation returns a new AppError as an error
func InputValidation(err error) error {
	return &AppError{codeInputValidation, err.Error(), http.StatusBadRequest}
}

// NotFound returns a new AppError as an error
func NotFound(resource string) error {
	return &AppError{codeNotFound, fmt.Sprintf("%s not found", resource), http.StatusNotFound}
}

// AlreadyExists returns a new AppError as an error
func AlreadyExists(resource string) error {
	return &AppError{codeAlreadyExists, fmt.Sprintf("%s already exists", resource), http.StatusConflict}
}

// PreconditionFailed returns a new AppError as an error
func PreconditionFailed(message string) error {
	return &AppError{codePreconditionFailed, message, http.StatusPreconditionFailed}
}
