package main

import (
	"andybar2/library-backend/src/cmd"
)

func main() {
	cmd.Execute()
}
