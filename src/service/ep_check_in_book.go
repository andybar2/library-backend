package service

import (
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/labstack/echo"

	"andybar2/library-backend/src/enums"
	"andybar2/library-backend/src/errors"
	"andybar2/library-backend/src/store"
)

// CheckInBookInput represents the input for the CheckInBook endpoint
type CheckInBookInput struct {
	ID     string `json:"id" validate:"required,uuid"`
	Person string `json:"person" validate:"required"`
}

// CheckInBookOutput represents the output for the CheckInBook endpoint
type CheckInBookOutput struct {
	Book *store.Book `json:"book"`
}

// CheckInBook implements the business logic for the endpoint
func (s *Service) CheckInBook(input *CheckInBookInput) (*CheckInBookOutput, error) {
	// Read book
	book, err := s.store.ReadBook(input.ID)
	if err != nil {
		return nil, err
	}

	// Create event in the history of the book and update its current state
	event := &store.HistoryEvent{
		ID:     uuid.New().String(),
		BookID: input.ID,
		Person: input.Person,
		Action: enums.BookActionCheckIn,
		Date:   time.Now(),
	}

	if err := s.store.CreateHistoryEvent(event); err != nil {
		return nil, err
	}

	book.CurrentState = enums.BookStateAvailable

	return &CheckInBookOutput{
		Book: book,
	}, nil
}

// MakeCheckInBookEndpoint returns an echo handler for the CheckInBook endpoint
func (s *Service) MakeCheckInBookEndpoint() echo.HandlerFunc {
	return func(ctx echo.Context) error {
		input := &CheckInBookInput{}

		if err := ctx.Bind(input); err != nil {
			return err
		}

		if err := ctx.Validate(input); err != nil {
			return errors.InputValidation(err)
		}

		output, err := s.CheckInBook(input)
		if err != nil {
			return err
		}

		return ctx.JSON(http.StatusOK, output)
	}
}
