package service_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"andybar2/library-backend/src/enums"
	"andybar2/library-backend/src/service"
)

func TestService_CheckInBook(t *testing.T) {
	t.Run("Successfully checks in a book", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book
		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)

		// Check out book
		checkOutBookInput := &service.CheckOutBookInput{
			ID:     createBookOutput.Book.ID,
			Person: "Test Person",
		}

		_, err = svc.CheckOutBook(checkOutBookInput)
		require.NoError(t, err)

		// Check in book
		checkInBookInput := &service.CheckInBookInput{
			ID:     createBookOutput.Book.ID,
			Person: "Test Person",
		}

		checkInBookOutput, err := svc.CheckInBook(checkInBookInput)
		require.NoError(t, err)
		require.NotNil(t, checkInBookOutput.Book)
		require.Equal(t, createBookOutput.Book.ID, checkInBookOutput.Book.ID)
		require.Equal(t, createBookOutput.Book.Title, checkInBookOutput.Book.Title)
		require.Equal(t, createBookOutput.Book.Author, checkInBookOutput.Book.Author)
		require.Equal(t, createBookOutput.Book.ISBN, checkInBookOutput.Book.ISBN)
		require.Equal(t, createBookOutput.Book.Description, checkInBookOutput.Book.Description)
		require.Equal(t, enums.BookStateAvailable, checkInBookOutput.Book.CurrentState)
	})

	t.Run("Returns an error if trying to check in a book that doesn't exist", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		checkInBookInput := &service.CheckInBookInput{
			ID:     uuid.New().String(),
			Person: "Test Person",
		}

		_, err := svc.CheckInBook(checkInBookInput)
		require.Error(t, err)
	})

	t.Run("Returns an error if trying to check in a book that is already available", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book
		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)

		// Check in book
		checkInBookInput := &service.CheckInBookInput{
			ID:     createBookOutput.Book.ID,
			Person: "Test Person",
		}

		_, err = svc.CheckInBook(checkInBookInput)
		require.Error(t, err)
	})
}
