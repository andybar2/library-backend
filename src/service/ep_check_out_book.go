package service

import (
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/labstack/echo"

	"andybar2/library-backend/src/enums"
	"andybar2/library-backend/src/errors"
	"andybar2/library-backend/src/store"
)

// CheckOutBookInput represents the input for the CheckOutBook endpoint
type CheckOutBookInput struct {
	ID     string `json:"id" validate:"required,uuid"`
	Person string `json:"person" validate:"required"`
}

// CheckOutBookOutput represents the output for the CheckOutBook endpoint
type CheckOutBookOutput struct {
	Book *store.Book `json:"book"`
}

// CheckOutBook implements the business logic for the endpoint
func (s *Service) CheckOutBook(input *CheckOutBookInput) (*CheckOutBookOutput, error) {
	// Read book
	book, err := s.store.ReadBook(input.ID)
	if err != nil {
		return nil, err
	}

	// Create event in the history of the book and update its current state
	event := &store.HistoryEvent{
		ID:     uuid.New().String(),
		BookID: input.ID,
		Person: input.Person,
		Action: enums.BookActionCheckOut,
		Date:   time.Now(),
	}

	if err := s.store.CreateHistoryEvent(event); err != nil {
		return nil, err
	}

	book.CurrentState = enums.BookStateTaken

	return &CheckOutBookOutput{
		Book: book,
	}, nil
}

// MakeCheckOutBookEndpoint returns an echo handler for the CheckOutBook endpoint
func (s *Service) MakeCheckOutBookEndpoint() echo.HandlerFunc {
	return func(ctx echo.Context) error {
		input := &CheckOutBookInput{}

		if err := ctx.Bind(input); err != nil {
			return err
		}

		if err := ctx.Validate(input); err != nil {
			return errors.InputValidation(err)
		}

		output, err := s.CheckOutBook(input)
		if err != nil {
			return err
		}

		return ctx.JSON(http.StatusOK, output)
	}
}
