package service_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"andybar2/library-backend/src/enums"
	"andybar2/library-backend/src/service"
)

func TestService_CheckOutBook(t *testing.T) {
	t.Run("Successfully checks out a book", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book
		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)

		// Check out book
		checkOutBookInput := &service.CheckOutBookInput{
			ID:     createBookOutput.Book.ID,
			Person: "Test Person",
		}

		checkOutBookOutput, err := svc.CheckOutBook(checkOutBookInput)
		require.NoError(t, err)
		require.NotNil(t, checkOutBookOutput.Book)
		require.Equal(t, createBookOutput.Book.ID, checkOutBookOutput.Book.ID)
		require.Equal(t, createBookOutput.Book.Title, checkOutBookOutput.Book.Title)
		require.Equal(t, createBookOutput.Book.Author, checkOutBookOutput.Book.Author)
		require.Equal(t, createBookOutput.Book.ISBN, checkOutBookOutput.Book.ISBN)
		require.Equal(t, createBookOutput.Book.Description, checkOutBookOutput.Book.Description)
		require.Equal(t, enums.BookStateTaken, checkOutBookOutput.Book.CurrentState)
	})

	t.Run("Returns an error if trying to check out a book that doesn't exist", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		checkOutBookInput := &service.CheckOutBookInput{
			ID:     uuid.New().String(),
			Person: "Test Person",
		}

		_, err := svc.CheckOutBook(checkOutBookInput)
		require.Error(t, err)
	})

	t.Run("Returns an error if trying to check out a book that is already taken", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book
		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)

		// Check out book
		checkOutBookInput := &service.CheckOutBookInput{
			ID:     createBookOutput.Book.ID,
			Person: "Test Person",
		}

		_, err = svc.CheckOutBook(checkOutBookInput)
		require.NoError(t, err)

		// check out book again
		_, err = svc.CheckOutBook(checkOutBookInput)
		require.Error(t, err)
	})
}
