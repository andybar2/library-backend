package service

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo"

	"andybar2/library-backend/src/enums"
	"andybar2/library-backend/src/errors"
	"andybar2/library-backend/src/store"
)

// CreateBookInput represents the input for the CreateBook endpoint
type CreateBookInput struct {
	Title       string `json:"title" validate:"required"`
	Author      string `json:"author" validate:"required"`
	ISBN        string `json:"isbn" validate:"required,isbn"`
	Description string `json:"description" validate:"required"`
}

// CreateBookOutput represents the output for the CreateBook endpoint
type CreateBookOutput struct {
	Book *store.Book `json:"book"`
}

// CreateBook implements the business logic for the endpoint
func (s *Service) CreateBook(input *CreateBookInput) (*CreateBookOutput, error) {
	book := &store.Book{
		ID:           uuid.New().String(),
		Title:        input.Title,
		Author:       input.Author,
		ISBN:         input.ISBN,
		Description:  input.Description,
		CurrentState: enums.BookStateAvailable,
	}

	if err := s.store.CreateBook(book); err != nil {
		return nil, err
	}

	return &CreateBookOutput{
		Book: book,
	}, nil
}

// MakeCreateBookEndpoint returns an echo handler for the CreateBook endpoint
func (s *Service) MakeCreateBookEndpoint() echo.HandlerFunc {
	return func(ctx echo.Context) error {
		input := &CreateBookInput{}

		if err := ctx.Bind(input); err != nil {
			return err
		}

		if err := ctx.Validate(input); err != nil {
			return errors.InputValidation(err)
		}

		output, err := s.CreateBook(input)
		if err != nil {
			return err
		}

		return ctx.JSON(http.StatusOK, output)
	}
}
