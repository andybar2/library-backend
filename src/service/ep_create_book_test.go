package service_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"andybar2/library-backend/src/enums"
	"andybar2/library-backend/src/service"
)

func TestService_CreateBook(t *testing.T) {
	t.Run("Successfully creates a book", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)
		require.NotNil(t, createBookOutput.Book)
		require.NotZero(t, createBookOutput.Book.ID)
		require.Equal(t, createBookInput.Title, createBookOutput.Book.Title)
		require.Equal(t, createBookInput.Author, createBookOutput.Book.Author)
		require.Equal(t, createBookInput.ISBN, createBookOutput.Book.ISBN)
		require.Equal(t, createBookInput.Description, createBookOutput.Book.Description)
		require.Equal(t, enums.BookStateAvailable, createBookOutput.Book.CurrentState)
	})

	t.Run("Returns an error if trying to create two books with the same ISBN", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		_, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)

		_, err = svc.CreateBook(createBookInput)
		require.Error(t, err)
	})
}
