package service

import (
	"net/http"

	"github.com/labstack/echo"

	"andybar2/library-backend/src/errors"
)

// DeleteBookInput represents the input for the DeleteBook endpoint
type DeleteBookInput struct {
	ID string `json:"id" validate:"required,uuid"`
}

// DeleteBookOutput represents the output for the DeleteBook endpoint
type DeleteBookOutput struct{}

// DeleteBook implements the business logic for the endpoint
func (s *Service) DeleteBook(input *DeleteBookInput) (*DeleteBookOutput, error) {
	if err := s.store.DeleteBook(input.ID); err != nil {
		return nil, err
	}

	return &DeleteBookOutput{}, nil
}

// MakeDeleteBookEndpoint returns an echo handler for the DeleteBook endpoint
func (s *Service) MakeDeleteBookEndpoint() echo.HandlerFunc {
	return func(ctx echo.Context) error {
		input := &DeleteBookInput{}

		if err := ctx.Bind(input); err != nil {
			return err
		}

		if err := ctx.Validate(input); err != nil {
			return errors.InputValidation(err)
		}

		output, err := s.DeleteBook(input)
		if err != nil {
			return err
		}

		return ctx.JSON(http.StatusOK, output)
	}
}
