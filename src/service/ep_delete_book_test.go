package service_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"andybar2/library-backend/src/service"
)

func TestService_DeleteBook(t *testing.T) {
	t.Run("Successfully deletes a book", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book
		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)

		// Delete book
		deleteBookInput := &service.DeleteBookInput{
			ID: createBookOutput.Book.ID,
		}

		_, err = svc.DeleteBook(deleteBookInput)
		require.NoError(t, err)
	})

	t.Run("Returns an error if trying to delete a book that doesn't exist", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		deleteBookInput := &service.DeleteBookInput{
			ID: uuid.New().String(),
		}

		_, err := svc.DeleteBook(deleteBookInput)
		require.Error(t, err)
	})
}
