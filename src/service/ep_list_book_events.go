package service

import (
	"net/http"

	"github.com/labstack/echo"

	"andybar2/library-backend/src/errors"
	"andybar2/library-backend/src/store"
)

// ListBookEventsInput represents the input for the ListBookEvents endpoint
type ListBookEventsInput struct {
	ID     string `json:"id" validate:"required,uuid"`
	Limit  int    `json:"limit" validate:"required,min=1,max=100"`
	Offset int    `json:"offset" validate:"omitempty,min=1"`
}

// ListBookEventsOutput represents the output for the ListBookEvents endpoint
type ListBookEventsOutput struct {
	Events []*store.HistoryEvent `json:"events"`
}

// ListBookEvents implements the business logic for the endpoint
func (s *Service) ListBookEvents(input *ListBookEventsInput) (*ListBookEventsOutput, error) {
	events, err := s.store.ListHistoryEvents(input.ID, input.Limit, input.Offset)
	if err != nil {
		return nil, err
	}

	return &ListBookEventsOutput{
		Events: events,
	}, nil
}

// MakeListBookEventsEndpoint returns an echo handler for the ListBookEvents endpoint
func (s *Service) MakeListBookEventsEndpoint() echo.HandlerFunc {
	return func(ctx echo.Context) error {
		input := &ListBookEventsInput{}

		if err := ctx.Bind(input); err != nil {
			return err
		}

		if err := ctx.Validate(input); err != nil {
			return errors.InputValidation(err)
		}

		output, err := s.ListBookEvents(input)
		if err != nil {
			return err
		}

		return ctx.JSON(http.StatusOK, output)
	}
}
