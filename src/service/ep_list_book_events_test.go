package service_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"andybar2/library-backend/src/enums"
	"andybar2/library-backend/src/service"
)

func TestService_ListBookEvents(t *testing.T) {
	t.Run("Successfully lists book events in descending order", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book
		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)

		// Create check out/in events
		for i := 0; i < 5; i++ {
			if i%2 == 0 {
				// Check out book
				checkOutBookInput := &service.CheckOutBookInput{
					ID:     createBookOutput.Book.ID,
					Person: "Test Person",
				}

				_, err = svc.CheckOutBook(checkOutBookInput)
				require.NoError(t, err)
			} else {
				// Check in book
				checkInBookInput := &service.CheckInBookInput{
					ID:     createBookOutput.Book.ID,
					Person: "Test Person",
				}

				_, err := svc.CheckInBook(checkInBookInput)
				require.NoError(t, err)
			}

			// small sleep between state changes, just to avoid dates' collisions
			time.Sleep(10 * time.Millisecond)
		}

		// List book events
		listBookEventsInput := &service.ListBookEventsInput{
			ID:    createBookOutput.Book.ID,
			Limit: 10,
		}

		listBookEventsOutput, err := svc.ListBookEvents(listBookEventsInput)
		require.NoError(t, err)
		require.Equal(t, 5, len(listBookEventsOutput.Events))

		for i := 0; i < 5; i++ {
			if i%2 == 0 {
				require.Equal(t, enums.BookActionCheckOut, listBookEventsOutput.Events[i].Action)
			} else {
				require.Equal(t, enums.BookActionCheckIn, listBookEventsOutput.Events[i].Action)
			}

			if i < 4 {
				require.True(t, listBookEventsOutput.Events[i].Date.After(listBookEventsOutput.Events[i+1].Date))
			}
		}
	})

	t.Run("Filters the list of book events by book id", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book 1
		createBookInput1 := &service.CreateBookInput{
			Title:       "Test Book 1",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput1, err := svc.CreateBook(createBookInput1)
		require.NoError(t, err)

		// Check out book 1
		checkOutBookInput1 := &service.CheckOutBookInput{
			ID:     createBookOutput1.Book.ID,
			Person: "Test Person",
		}

		_, err = svc.CheckOutBook(checkOutBookInput1)
		require.NoError(t, err)

		// Create book 2
		createBookInput2 := &service.CreateBookInput{
			Title:       "Test Book 2",
			Author:      "Test Author",
			ISBN:        "978-3-598-21501-8",
			Description: "This is just a book for testing.",
		}

		createBookOutput2, err := svc.CreateBook(createBookInput2)
		require.NoError(t, err)

		// Check out book 2
		checkOutBookInput2 := &service.CheckOutBookInput{
			ID:     createBookOutput2.Book.ID,
			Person: "Test Person",
		}

		_, err = svc.CheckOutBook(checkOutBookInput2)
		require.NoError(t, err)

		// List book events for book 1
		listBookEventsInput := &service.ListBookEventsInput{
			ID:    createBookOutput1.Book.ID,
			Limit: 10,
		}

		listBookEventsOutput, err := svc.ListBookEvents(listBookEventsInput)
		require.NoError(t, err)
		require.Equal(t, 1, len(listBookEventsOutput.Events))
		require.Equal(t, createBookOutput1.Book.ID, listBookEventsOutput.Events[0].BookID)
	})

	t.Run("Paginates the list of book events", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book
		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)

		// Create check out/in events
		for i := 0; i < 5; i++ {
			if i%2 == 0 {
				// Check out book
				checkOutBookInput := &service.CheckOutBookInput{
					ID:     createBookOutput.Book.ID,
					Person: "Test Person",
				}

				_, err = svc.CheckOutBook(checkOutBookInput)
				require.NoError(t, err)
			} else {
				// Check in book
				checkInBookInput := &service.CheckInBookInput{
					ID:     createBookOutput.Book.ID,
					Person: "Test Person",
				}

				_, err := svc.CheckInBook(checkInBookInput)
				require.NoError(t, err)
			}

			// small sleep between state changes, just to avoid dates' collisions
			time.Sleep(10 * time.Millisecond)
		}

		// List first page of book events
		listBookEventsInput := &service.ListBookEventsInput{
			ID:     createBookOutput.Book.ID,
			Limit:  2,
			Offset: 0,
		}

		listBookEventsOutput1, err := svc.ListBookEvents(listBookEventsInput)
		require.NoError(t, err)
		require.Equal(t, 2, len(listBookEventsOutput1.Events))

		// List second page of book events
		listBookEventsInput = &service.ListBookEventsInput{
			ID:     createBookOutput.Book.ID,
			Limit:  2,
			Offset: 2,
		}

		listBookEventsOutput2, err := svc.ListBookEvents(listBookEventsInput)
		require.NoError(t, err)
		require.Equal(t, 2, len(listBookEventsOutput2.Events))

		require.True(t, listBookEventsOutput1.Events[0].Date.After(listBookEventsOutput1.Events[1].Date))
		require.True(t, listBookEventsOutput1.Events[1].Date.After(listBookEventsOutput2.Events[0].Date))
		require.True(t, listBookEventsOutput2.Events[0].Date.After(listBookEventsOutput2.Events[1].Date))
	})
}
