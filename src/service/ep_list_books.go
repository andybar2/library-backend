package service

import (
	"net/http"

	"github.com/labstack/echo"

	"andybar2/library-backend/src/errors"
	"andybar2/library-backend/src/store"
)

// ListBooksInput represents the input for the ListBooks endpoint
type ListBooksInput struct {
	Title  string `json:"title" validate:"-"`
	Limit  int    `json:"limit" validate:"required,min=1,max=100"`
	Offset int    `json:"offset" validate:"omitempty,min=1"`
}

// ListBooksOutput represents the output for the ListBooks endpoint
type ListBooksOutput struct {
	Books []*store.Book `json:"books"`
}

// ListBooks implements the business logic for the endpoint
func (s *Service) ListBooks(input *ListBooksInput) (*ListBooksOutput, error) {
	books, err := s.store.ListBooks(input.Title, input.Limit, input.Offset)
	if err != nil {
		return nil, err
	}

	return &ListBooksOutput{
		Books: books,
	}, nil
}

// MakeListBooksEndpoint returns an echo handler for the ListBooks endpoint
func (s *Service) MakeListBooksEndpoint() echo.HandlerFunc {
	return func(ctx echo.Context) error {
		input := &ListBooksInput{}

		if err := ctx.Bind(input); err != nil {
			return err
		}

		if err := ctx.Validate(input); err != nil {
			return errors.InputValidation(err)
		}

		output, err := s.ListBooks(input)
		if err != nil {
			return err
		}

		return ctx.JSON(http.StatusOK, output)
	}
}
