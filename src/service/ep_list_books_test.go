package service_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"

	"andybar2/library-backend/src/service"
)

func TestService_ListBooks(t *testing.T) {
	t.Run("Successfully lists books", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create books
		ids := []string{}

		for i := 0; i < 5; i++ {
			createBookInput := &service.CreateBookInput{
				Title:       fmt.Sprintf("Test Book %v", i),
				Author:      "Test Author",
				ISBN:        fmt.Sprintf("ISBN %v", i),
				Description: "This is just a book for testing.",
			}

			createBookOutput, err := svc.CreateBook(createBookInput)
			require.NoError(t, err)

			ids = append(ids, createBookOutput.Book.ID)
		}

		// List books
		listBooksInput := &service.ListBooksInput{
			Limit: 10,
		}

		listBooksOutput, err := svc.ListBooks(listBooksInput)
		require.NoError(t, err)
		require.Equal(t, 5, len(listBooksOutput.Books))

		for i := 0; i < 5; i++ {
			require.Equal(t, ids[i], listBooksOutput.Books[i].ID)
		}
	})

	t.Run("Filters the list of books by title", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create books
		ids := []string{}

		for i := 0; i < 5; i++ {
			prefix := "Test"

			if i%2 == 0 {
				prefix = "Other"
			}

			createBookInput := &service.CreateBookInput{
				Title:       fmt.Sprintf("%s Book %v", prefix, i),
				Author:      "Test Author",
				ISBN:        fmt.Sprintf("ISBN %v", i),
				Description: "This is just a book for testing.",
			}

			createBookOutput, err := svc.CreateBook(createBookInput)
			require.NoError(t, err)

			ids = append(ids, createBookOutput.Book.ID)
		}

		// List books
		listBooksInput := &service.ListBooksInput{
			Title: "test",
			Limit: 10,
		}

		listBooksOutput, err := svc.ListBooks(listBooksInput)
		require.NoError(t, err)
		require.Equal(t, 2, len(listBooksOutput.Books))
		require.Equal(t, ids[1], listBooksOutput.Books[0].ID)
		require.Equal(t, ids[3], listBooksOutput.Books[1].ID)
	})

	t.Run("Paginates the list of books", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create books
		ids := []string{}

		for i := 0; i < 5; i++ {
			createBookInput := &service.CreateBookInput{
				Title:       fmt.Sprintf("Test Book %v", i),
				Author:      "Test Author",
				ISBN:        fmt.Sprintf("ISBN %v", i),
				Description: "This is just a book for testing.",
			}

			createBookOutput, err := svc.CreateBook(createBookInput)
			require.NoError(t, err)

			ids = append(ids, createBookOutput.Book.ID)
		}

		// List first page of books
		listBooksInput := &service.ListBooksInput{
			Limit:  2,
			Offset: 0,
		}

		listBooksOutput, err := svc.ListBooks(listBooksInput)
		require.NoError(t, err)
		require.Equal(t, 2, len(listBooksOutput.Books))
		require.Equal(t, ids[0], listBooksOutput.Books[0].ID)
		require.Equal(t, ids[1], listBooksOutput.Books[1].ID)

		// List second page of books
		listBooksInput = &service.ListBooksInput{
			Limit:  2,
			Offset: 2,
		}

		listBooksOutput, err = svc.ListBooks(listBooksInput)
		require.NoError(t, err)
		require.Equal(t, 2, len(listBooksOutput.Books))
		require.Equal(t, ids[2], listBooksOutput.Books[0].ID)
		require.Equal(t, ids[3], listBooksOutput.Books[1].ID)
	})
}
