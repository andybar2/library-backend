package service

import (
	"net/http"

	"github.com/labstack/echo"

	"andybar2/library-backend/src/errors"
	"andybar2/library-backend/src/store"
)

// ReadBookInput represents the input for the ReadBook endpoint
type ReadBookInput struct {
	ID string `json:"id" validate:"required,uuid"`
}

// ReadBookOutput represents the output for the ReadBook endpoint
type ReadBookOutput struct {
	Book *store.Book `json:"book"`
}

// ReadBook implements the business logic for the endpoint
func (s *Service) ReadBook(input *ReadBookInput) (*ReadBookOutput, error) {
	book, err := s.store.ReadBook(input.ID)
	if err != nil {
		return nil, err
	}

	return &ReadBookOutput{
		Book: book,
	}, nil
}

// MakeReadBookEndpoint returns an echo handler for the ReadBook endpoint
func (s *Service) MakeReadBookEndpoint() echo.HandlerFunc {
	return func(ctx echo.Context) error {
		input := &ReadBookInput{}

		if err := ctx.Bind(input); err != nil {
			return err
		}

		if err := ctx.Validate(input); err != nil {
			return errors.InputValidation(err)
		}

		output, err := s.ReadBook(input)
		if err != nil {
			return err
		}

		return ctx.JSON(http.StatusOK, output)
	}
}
