package service_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"andybar2/library-backend/src/service"
)

func TestService_ReadBook(t *testing.T) {
	t.Run("Successfully reads a book", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book
		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)

		// Read book
		readBookInput := &service.ReadBookInput{
			ID: createBookOutput.Book.ID,
		}

		readBookOutput, err := svc.ReadBook(readBookInput)
		require.NoError(t, err)
		require.NotNil(t, readBookOutput.Book)
		require.Equal(t, createBookOutput.Book.ID, readBookOutput.Book.ID)
		require.Equal(t, createBookOutput.Book.Title, readBookOutput.Book.Title)
		require.Equal(t, createBookOutput.Book.Author, readBookOutput.Book.Author)
		require.Equal(t, createBookOutput.Book.ISBN, readBookOutput.Book.ISBN)
		require.Equal(t, createBookOutput.Book.Description, readBookOutput.Book.Description)
		require.Equal(t, createBookOutput.Book.CurrentState, readBookOutput.Book.CurrentState)
	})

	t.Run("Returns an error if trying to read a book that doesn't exist", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		readBookInput := &service.ReadBookInput{
			ID: uuid.New().String(),
		}

		_, err := svc.ReadBook(readBookInput)
		require.Error(t, err)
	})
}
