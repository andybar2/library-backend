package service

import (
	"net/http"

	"github.com/labstack/echo"

	"andybar2/library-backend/src/errors"
	"andybar2/library-backend/src/store"
)

// UpdateBookInput represents the input for the UpdateBook endpoint
type UpdateBookInput struct {
	ID          string `json:"id" validate:"required,uuid"`
	Title       string `json:"title" validate:"required"`
	Author      string `json:"author" validate:"required"`
	ISBN        string `json:"isbn" validate:"required,isbn"`
	Description string `json:"description" validate:"required"`
}

// UpdateBookOutput represents the output for the UpdateBook endpoint
type UpdateBookOutput struct {
	Book *store.Book `json:"book"`
}

// UpdateBook implements the business logic for the endpoint
func (s *Service) UpdateBook(input *UpdateBookInput) (*UpdateBookOutput, error) {
	// Read book
	book, err := s.store.ReadBook(input.ID)
	if err != nil {
		return nil, err
	}

	// Update book
	book.Title = input.Title
	book.Author = input.Author
	book.ISBN = input.ISBN
	book.Description = input.Description

	if err := s.store.UpdateBook(book); err != nil {
		return nil, err
	}

	return &UpdateBookOutput{
		Book: book,
	}, nil
}

// MakeUpdateBookEndpoint returns an echo handler for the UpdateBook endpoint
func (s *Service) MakeUpdateBookEndpoint() echo.HandlerFunc {
	return func(ctx echo.Context) error {
		input := &UpdateBookInput{}

		if err := ctx.Bind(input); err != nil {
			return err
		}

		if err := ctx.Validate(input); err != nil {
			return errors.InputValidation(err)
		}

		output, err := s.UpdateBook(input)
		if err != nil {
			return err
		}

		return ctx.JSON(http.StatusOK, output)
	}
}
