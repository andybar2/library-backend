package service_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"andybar2/library-backend/src/service"
)

func TestService_UpdateBook(t *testing.T) {
	t.Run("Successfully updates a book", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book
		createBookInput := &service.CreateBookInput{
			Title:       "Test Book",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput, err := svc.CreateBook(createBookInput)
		require.NoError(t, err)

		// Update book
		updateBookInput := &service.UpdateBookInput{
			ID:          createBookOutput.Book.ID,
			Title:       "New Title",
			Author:      "New Author",
			ISBN:        "978-3-598-21501-8",
			Description: "New description.",
		}

		updateBookOutput, err := svc.UpdateBook(updateBookInput)
		require.NoError(t, err)
		require.NotNil(t, updateBookOutput.Book)
		require.Equal(t, updateBookInput.ID, updateBookOutput.Book.ID)
		require.Equal(t, updateBookInput.Title, updateBookOutput.Book.Title)
		require.Equal(t, updateBookInput.Author, updateBookOutput.Book.Author)
		require.Equal(t, updateBookInput.ISBN, updateBookOutput.Book.ISBN)
		require.Equal(t, updateBookInput.Description, updateBookOutput.Book.Description)
		require.Equal(t, createBookOutput.Book.CurrentState, updateBookOutput.Book.CurrentState)
	})

	t.Run("Returns an error if trying to update a book that doesn't exist", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		updateBookInput := &service.UpdateBookInput{
			ID:          uuid.New().String(),
			Title:       "New Title",
			Author:      "New Author",
			ISBN:        "978-3-598-21501-8",
			Description: "New description.",
		}

		_, err := svc.UpdateBook(updateBookInput)
		require.Error(t, err)
	})

	t.Run("Returns an error if trying to set an existing ISBN to another book", func(t *testing.T) {
		svc, reset := newTestService(t)
		defer reset()

		// Create book 1
		createBookInput1 := &service.CreateBookInput{
			Title:       "Test Book 1",
			Author:      "Test Author",
			ISBN:        "978-3-598-21500-1",
			Description: "This is just a book for testing.",
		}

		createBookOutput1, err := svc.CreateBook(createBookInput1)
		require.NoError(t, err)

		// Create book 2
		createBookInput2 := &service.CreateBookInput{
			Title:       "Test Book 2",
			Author:      "Test Author",
			ISBN:        "978-3-598-21501-8",
			Description: "This is just a book for testing.",
		}

		_, err = svc.CreateBook(createBookInput2)
		require.NoError(t, err)

		// Update book 1 with the ISBN of book 2
		updateBookInput := &service.UpdateBookInput{
			ID:          createBookOutput1.Book.ID,
			Title:       "New Title",
			Author:      "New Author",
			ISBN:        createBookInput2.ISBN,
			Description: "New description.",
		}

		_, err = svc.UpdateBook(updateBookInput)
		require.Error(t, err)
	})
}
