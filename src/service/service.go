package service

import (
	"net/http"

	"github.com/facebookgo/grace/gracehttp"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"

	"andybar2/library-backend/src/config"
	"andybar2/library-backend/src/errors"
	"andybar2/library-backend/src/store"
	"andybar2/library-backend/src/validator"
)

// Service implements the backend endpoints
type Service struct {
	conf  *config.Config
	store *store.Store
	echo  *echo.Echo
}

// New returns a new Service
func New(conf *config.Config, st *store.Store) *Service {
	// Server
	e := echo.New()
	e.Server.Addr = ":" + conf.HTTPPort
	e.HTTPErrorHandler = errors.Handler()
	e.Validator = validator.New()

	// Service
	svc := &Service{
		conf:  conf,
		store: st,
		echo:  e,
	}

	// Logger
	switch conf.LogLevel {
	case "debug":
		e.Logger.SetLevel(log.DEBUG)
	case "info":
		e.Logger.SetLevel(log.INFO)
	case "warn":
		e.Logger.SetLevel(log.WARN)
	case "error":
		e.Logger.SetLevel(log.ERROR)
	default:
		e.Logger.SetLevel(log.OFF)
	}

	// Middlewares
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
		AllowMethods: []string{http.MethodOptions, http.MethodPost},
	}))

	// Routes
	e.POST("/books/create", svc.MakeCreateBookEndpoint())
	e.POST("/books/read", svc.MakeReadBookEndpoint())
	e.POST("/books/update", svc.MakeUpdateBookEndpoint())
	e.POST("/books/delete", svc.MakeDeleteBookEndpoint())
	e.POST("/books/list", svc.MakeListBooksEndpoint())
	e.POST("/books/events/check-out", svc.MakeCheckOutBookEndpoint())
	e.POST("/books/events/check-in", svc.MakeCheckInBookEndpoint())
	e.POST("/books/events/list", svc.MakeListBookEventsEndpoint())

	return svc
}

// Run executes the service
func (s *Service) Run() error {
	s.echo.Logger.Infof("Starting service on %s", s.echo.Server.Addr)
	return gracehttp.Serve(s.echo.Server)
}
