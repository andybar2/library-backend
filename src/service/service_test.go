package service_test

import (
	"testing"

	"github.com/jmoiron/sqlx"

	"andybar2/library-backend/src/config"
	"andybar2/library-backend/src/service"
	"andybar2/library-backend/src/store"
)

// newTestService returns a Service for testing and a function to clean up its database
func newTestService(t *testing.T) (*service.Service, func()) {
	t.Helper()

	// Config
	conf := config.New()

	// Store
	st, err := store.New(conf)
	if err != nil {
		t.Fatal(err.Error())
	}

	// Service
	svc := service.New(conf, st)

	return svc, func() {
		cleanDB(t, st.DB)
		st.Close()
	}
}

// cleanDB cleans all the tables in the database
func cleanDB(t *testing.T, db *sqlx.DB) {
	t.Helper()

	if _, err := db.Exec("DELETE FROM books;"); err != nil {
		t.Fatal(err.Error())
	}
}
