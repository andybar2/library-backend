package store

import (
	"database/sql"
	"strings"
)

// Book represents a row in the books table
type Book struct {
	ID           string `json:"id" db:"id"`
	Title        string `json:"title" db:"title"`
	Author       string `json:"author" db:"author"`
	ISBN         string `json:"isbn" db:"isbn"`
	Description  string `json:"description" db:"description"`
	CurrentState int    `json:"current_state" db:"current_state"`
}

// CreateBook inserts a new book in the database
func (s *Store) CreateBook(book *Book) error {
	query := `
		INSERT INTO books (
			id,
			title,
			author,
			isbn,
			description,
			current_state
		)
		VALUES (
			:id,
			:title,
			:author,
			:isbn,
			:description,
			:current_state
		);
	`

	stmt, err := s.DB.PrepareNamed(query)
	if err != nil {
		return err
	}

	if _, err := stmt.Exec(book); err != nil {
		return err
	}

	return nil
}

// ReadBook reads a book from the database
func (s *Store) ReadBook(id string) (*Book, error) {
	query := `
		SELECT
			*
		FROM
			books
		WHERE
			id = ?;
	`

	book := &Book{}

	if err := s.DB.Get(book, query, id); err != nil {
		return nil, err
	}

	return book, nil
}

// UpdateBook updates a book in the database without affecting the current state of it
func (s *Store) UpdateBook(book *Book) error {
	query := `
		UPDATE books
		SET
			title = :title,
			author = :author,
			isbn = :isbn,
			description = :description
		WHERE
			id = :id;
	`

	stmt, err := s.DB.PrepareNamed(query)
	if err != nil {
		return err
	}

	res, err := stmt.Exec(book)
	if err != nil {
		return err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err
	} else if rowsAffected == 0 {
		return sql.ErrNoRows
	}

	return nil
}

// DeleteBook deletes a book from the database
func (s *Store) DeleteBook(id string) error {
	query := `
		DELETE
		FROM
			books
		WHERE
			id = ?;
	`

	res, err := s.DB.Exec(query, id)
	if err != nil {
		return err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err
	} else if rowsAffected == 0 {
		return sql.ErrNoRows
	}

	return nil
}

// ListBooks lists books from the database
func (s *Store) ListBooks(title string, limit, offset int) ([]*Book, error) {
	query := `
		SELECT
			*
		FROM
			books
		WHERE
			title LIKE ?
		ORDER BY
			title, id
		LIMIT ?
		OFFSET ?;
	`

	titleFilter := strings.TrimSpace(title) + "%"
	books := []*Book{}

	err := s.DB.Select(&books, query, titleFilter, limit, offset)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}

	return books, nil
}
