package store

import (
	"database/sql"
	"time"

	"andybar2/library-backend/src/enums"
	"andybar2/library-backend/src/errors"

	"github.com/jmoiron/sqlx"
)

// HistoryEvent represents a row in the history table
type HistoryEvent struct {
	ID     string    `json:"id" db:"id"`
	BookID string    `json:"book_id" db:"book_id"`
	Person string    `json:"person" db:"person"`
	Action int       `json:"action" db:"action"`
	Date   time.Time `json:"date" db:"date"`
}

// CreateHistoryEvent creates a new event in the history of a book and also updates the state of it in the database
func (s *Store) CreateHistoryEvent(event *HistoryEvent) (rErr error) {
	// Begin transaction
	tx, err := s.DB.Beginx()
	if err != nil {
		return err
	}
	defer func() {
		if rErr != nil {
			tx.Rollback()
		}
	}()

	// Get book and lock it for update
	book, err := getAndLockBook(tx, event.BookID)
	if err != nil {
		return err
	}

	if event.Action == enums.BookActionCheckOut && book.CurrentState == enums.BookStateTaken {
		return errors.PreconditionFailed("book is already taken")
	} else if event.Action == enums.BookActionCheckIn && book.CurrentState == enums.BookStateAvailable {
		return errors.PreconditionFailed("book is already available")
	}

	// Create event in the history of the book
	if err = createHistoryEvent(tx, event); err != nil {
		return err
	}

	// Update state of the book
	var state int

	if event.Action == enums.BookActionCheckOut {
		state = enums.BookStateTaken
	} else {
		state = enums.BookStateAvailable
	}

	if err = updateBookState(tx, event.BookID, state); err != nil {
		return err
	}

	// Commit transaction
	return tx.Commit()
}

// getAndLockBook returns a book by id and locks it for further updates
func getAndLockBook(tx *sqlx.Tx, id string) (*Book, error) {
	query := `
		SELECT
			*
		FROM
			books
		WHERE
			id = ?
		FOR UPDATE;
	`

	book := &Book{}

	if err := tx.Get(book, query, id); err != nil {
		return nil, err
	}

	return book, nil
}

// createHistoryEvent inserts a new event in the history of a book
func createHistoryEvent(tx *sqlx.Tx, event *HistoryEvent) error {
	query := `
		INSERT INTO history (
			id,
			book_id,
			person,
			action,
			date
		)
		VALUES (
			:id,
			:book_id,
			:person,
			:action,
			:date
		);
	`

	stmt, err := tx.PrepareNamed(query)
	if err != nil {
		return err
	}

	if _, err := stmt.Exec(event); err != nil {
		return err
	}

	return nil
}

// updateBookState updates the current state of a book in the database
func updateBookState(tx *sqlx.Tx, id string, state int) error {
	query := `
		UPDATE books
		SET
			current_state = ?
		WHERE
			id = ?;
	`

	res, err := tx.Exec(query, state, id)
	if err != nil {
		return err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err
	} else if rowsAffected == 0 {
		return sql.ErrNoRows
	}

	return nil
}

// ListHistoryEvents lists events of the history of a book from the database
func (s *Store) ListHistoryEvents(id string, limit, offset int) ([]*HistoryEvent, error) {
	query := `
		SELECT
			*
		FROM
			history
		WHERE
			book_id = ?
		ORDER BY
			book_id DESC, date DESC
		LIMIT ?
		OFFSET ?;
	`

	events := []*HistoryEvent{}

	err := s.DB.Select(&events, query, id, limit, offset)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}

	return events, nil
}
