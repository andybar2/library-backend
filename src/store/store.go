package store

import (
	"context"
	"fmt"
	"time"

	// Import mysql driver
	_ "github.com/go-sql-driver/mysql"

	"andybar2/library-backend/src/config"

	"github.com/jmoiron/sqlx"
)

// Store wraps an sql database
type Store struct {
	DB *sqlx.DB
}

// New returns a new Store connected to a sql database
func New(conf *config.Config) (*Store, error) {
	ctx := context.Background()
	driver := "mysql"
	url := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", conf.MySQLUser, conf.MySQLPassword, conf.MySQLHost, conf.MySQLPort, conf.MySQLDatabase)

	db, err := sqlx.ConnectContext(ctx, driver, url)
	if err != nil {
		// Retry connection in a few secs
		time.Sleep(5 * time.Second)

		db, err = sqlx.ConnectContext(ctx, driver, url)
		if err != nil {
			return nil, err
		}
	}

	return &Store{db}, err
}

// Close ends the connection with the database
func (s *Store) Close() {
	s.DB.Close()
}
