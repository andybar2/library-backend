package validator

import (
	val "gopkg.in/go-playground/validator.v9"
)

// Validator implements a custom validator for struct types
type Validator struct {
	validator *val.Validate
}

// New creates a new Validator
func New() *Validator {
	v := val.New()

	return &Validator{
		validator: v,
	}
}

// Validate executes the validations declared on the 'validate' tags of a given struct
func (v *Validator) Validate(i interface{}) error {
	return v.validator.Struct(i)
}
